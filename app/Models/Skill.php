<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * Job Skill
 *
 * @author nickmoline
 * @version 1.0.0
 * @since 1.0.0
 * @property integer $id Skill ID
 * @property \Carbon\Carbon $created_at Timestamp Posting Inserted into DB
 * @property \Carbon\Carbon $updated_at Timestamp Posting last updated in DB
 * @property \Carbon\Carbon $deleted_at Timestamp Posting Soft Deleted
 * @property string $name Name of Skill
 * @property string $slug Unique Slug for Skill
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Posting[] $postings

 */
class Skill extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'slug'];

    /**
     * Many-To-Many Relationship to Posting
     */
    public function postings()
    {
        return $this->belongsToMany('App\Models\Posting')->withTimestamps();
    }

    /**
     * Mutator to set slug when setting name
     *
     * @param string $value - value for the name property
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
