<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;

/**
 * Resource Controller for working with Skills
 * @TODO implement other REST endpoints to create/update/delete skills
 *
 * @author nickmoline
 * @version 1.0.0
 * @since 1.0.0
 */
class SkillController extends Controller
{
    /**
     * Return a list of all skills currently in the database
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Skill::orderBy('slug', 'asc')->get();
    }
}
