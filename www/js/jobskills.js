var skills = [];
$(document).ready(function() {
    $.get('api/skills', function(data) {
        $("#skillSelectDrop option").remove();
        $("#skillSelectDrop").append(
            '<option value="" style="font-weight: bold;">Select a Skill...</option>');
        data.forEach(function(skill) {
            skills[skill.slug] = skill.name;
            $("#skillSelectDrop").append('<option id="select-' + skill.slug +
                '" value="' + skill.slug + '">' + skill
                .name + '</option>');
        });
        console.log(skills);
    });

    $('#skillSelectDrop').change(function() {
        var skill = $("#skillSelectDrop").val();
        if (skill == "") {
            return;
        }

        var element = $('<div class="form-group row"><label for="skill-' + skill +
            '" class="col-sm-2 col-form-label">' + skills[skill] +
            '</label><div class="col-sm-9"><input type="range" id="skill-' +
            skill +
            '" name="' + skill +
            '" min="1" max="5" value="3" class="form-control-range" /></div><div class="col-sm-1" id="skill-' +
            skill + '-count">3 / 5</div></div>'
        );

        $("#skillSelectDrop").val("");
        $('#skillSelectDrop option[value="' + skill + '"]').remove();
        $("#skillContainer").append(element);
        element.change(updatePostingList);
        updatePostingList();
    });

    function updatePostingList() {
        var skillList = [];
        var queryString = "mode=json";
        $('#skillContainer input').each(function(index, element) {
            skillList[$(this).attr('name')] = $(this).val();
            $('#skill-' + $(this).attr('name') + '-count').text($(this).val() + ' / 5');
            queryString += '&skill[' + $(this).attr('name') + ']=' + $(this).val();

        });
        $.get('api/postings/search/skills', queryString, function(data) {
            $("#results-header").html('<h2>'+data.count+' Matching Job Postings Found</h2>')
            $("#results thead").html(
                '<tr><th>Company</th><th>Job</th><th>Score</th><th>Relevant Skills</th></tr>'
            );
            $('#results tbody').html('');
            console.log(data);
            data.results.forEach(function(posting) {
                $("#results tbody").append('<tr><th>' + posting.company.name +
                    '</th><td>' + posting
                    .title + '</td><td>' + posting.score + '</td><td><em>' + posting
                    .skill_list
                    .join(', ') + '</em></td></tr>');
            });
        });
    }

});
