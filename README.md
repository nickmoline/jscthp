### JSCTHP

- `php artisan migrate` will instantiate the database schema
- `php artisan populate:jobs` will populate the database with the data located in `storage/app/job_imports.csv`
- [jscthp.moline.dev](https://jscthp.moline.dev/)
