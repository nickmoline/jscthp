<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Job Search by Skill</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style type="text/css">
        .navbar select {
            background-color: #333;
            color: #bbb;
        }

        #skillContainer label {
            padding: 0;
            text-align: right;
            font-weight: bold;
        }

        #skillContainer input {
            margin-top: 5px;
        }
    </style>

</head>

<body>

    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Select a Skill:</a>
        <div class="navbar-collapse" id="navbarCollapse">
            <form class="form-inline" id="skillSelector">
                <select name="skillSelect" id="skillSelectDrop" class="form-control form-control-dark w-100"
                    placeholder="Select a Skill">
                    <option disabled>Loading...</option>
                </select>
            </form>
        </div>
    </nav>


    <div role="main" class="container">
        <div class="jumbotron">
            <h1>Rate Yourself on Skills</h1>
            <p>Add skills you have by selecting a skill from the dropdown above, and then rate your proficiency in that
                skill on a scale of 1 to 5 below.</p>
            <p>Relevant jobs requiring skills you possess will be displayed below.</p>
            <form id="skillScoreSet">
                <div id="skillContainer">

                </div>
            </form>
        </div>

        <div class="table-responsive" id="results">
            <div id="results-header"></div>
            <table class="table table-striped table-sm">
                <thead>

                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>

    <script src="/js/jobskills.js"></script>

</body>

</html>